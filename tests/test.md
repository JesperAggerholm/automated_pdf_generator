---
title: 'Template test'
subtitle: 'Markdown + Latex + pandoc'
authors: ['Jesper Aggerholm', 'Louis']
main_author: 'Louis'
date: \today
#institution: 
email: 'jesper@Vleppo.com'
left-header: \today
right-header: Template test
skip-toc: false
skip-lof: false
---

# Introduction

Simple implementation of automation! This really works!
And a footnote ^[Footnote example]

This is a list

* A
* B
    * sub B
* C

and some more text.

And this is a really really long line with lots of text and blabla to ensure that must be doing a certain amount of line wrapping. That was a lot.

# How does it handle strange characters?

* Danish letters: >æøåÆØÅ<
* Accents: >êéè<

# Image

We want images to be where they are mentioned in the text, like this

![this is a gitlab logo](logo.png){ height=25px }

# A bit about the preamble options

* title: 'Template test'
* subtitle: 'Markdown + Latex + pandoc'
* authors: ['Author Name 1', 'Author Name 2']
* main_author: 'Author Name 1'
* date: \today
* institution: Lillebealt Academy (default: "UCL university college")
* email: 'test@example.com'
* left-header: \today
* right-header: Template test
* skip-toc: false (Default: false)
* skip-lof: false (Default: false)
