#!/usr/bin/env bash

if [ "x" == "x$1" ]; then
	echo "usage: $0 <basename> <pdfname>"
	exit 1
fi

MDNAME=$1

if [ "x" == "x$2" ]; then
	PDFNAME=$MDNAME
else
	PDFNAME=$2
fi

# latex-engine renamed to pdf-engine
# this is a difference between stretch and buster...
# see https://github.com/jgm/pandoc/blob/d50f2a0bf684060f431348ebe2977f243162ec2b/changelog#L4349
PANDOC_VERSION=$(pandoc -v | head -n 1)
if [ "x_$PANDOC_VERSION" \< "x_pandoc 2" ]; then
	echo using \"--latex-engine\"
	ENGINE_SWITCH="latex-engine"
else
	echo using \"--pdf-engine\"
	ENGINE_SWITCH="pdf-engine"
fi

echo using pandoc to build $PDFNAME.pdf from $MDNAME.md
pandoc 	--from markdown  	\
	--to latex		\
	--template template/template.tex	\
        --out $MDNAME.pdf	\
	--$ENGINE_SWITCH pdflatex	\
	$MDNAME.md
